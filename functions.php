<?php
require_once( __DIR__ . '/vendor/autoload.php' );

$timber = new \Timber\Timber();

$sage_includes = [
  'lib/timber.php',
  'lib/assets.php',
	'lib/setup.php',
	'lib/class-post.php',
	'lib/amp.php'
];

foreach ( $sage_includes as $file ) {
  $filepath = locate_template( $file );
	if ( !$filepath  ) {
		trigger_error( sprintf( __( 'Error locating %s for inclusion', 'nroom' ), $file ), E_USER_ERROR );
	}

	require_once $filepath;
}

foreach (glob( __DIR__ . "/nroom_modules/*/*.php") as $filename) {
  require_once $filename;
}

foreach (glob( __DIR__ . "/blocks/*/*.php") as $filename) {
  require_once $filename;
}

function nroom_modules_path() {
	return get_stylesheet_directory_uri().'/nroom_modules\/';
}

function wpdocs_my_search_form( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
	<img class="searchform__close" src="'.home_url( '/' ).'/app/themes/nroom/dist/images/close.svg" />
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Zadaj pytanie, my odpowiemy i pokażemy najlepsze teksty" onfocus="this.placeholder = \'\'"onblur="this.placeholder = \'Zadaj pytanie, my odpowiemy i pokażemy najlepsze teksty\'" />
	<button type="submit" class="submit"><img src="'.home_url( '/' ).'/app/themes/nroom/dist/images/search2.svg" /></button>
	</form>';

	return $form;
}
add_filter( 'get_search_form', 'wpdocs_my_search_form' );

function myprefix_search_posts_per_page($query) {
	if ( $query->is_search ) {
			$query->set( 'posts_per_page', '21' );
	}
	return $query;
}
add_filter( 'pre_get_posts','myprefix_search_posts_per_page' );

add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
	global $post;
	if ( is_single() && get_field('ekstra_post', $post->ID) ) {
			$classes[] = 'post-extra';
	}
	if ( get_field('home', $post) ) {
			$classes[] = 'home';
	}
	return $classes;
}

add_theme_support( 'align-wide' );

function get_category_by_url($url) {
	foreach( (get_the_category()) as $category) {
		var_dump(get_category_link($category->cat_ID));
		if ( get_category_link($category->cat_ID) == $url ) {
			return $category->slug;
		}
	}
	return false;
}

function get_post_from_category($category_id) {
	$args = [
		'category__in' => $category_id,
		'posts_per_page' => 3
	];
	return Timber::get_posts($args, 'ThemePost');
}

add_filter( 'the_content', 'prefix_insert_post_ads' );
function prefix_insert_post_ads( $content ) {
  global $post;
	if ( is_single() && ! is_admin() ) {
		$content = str_replace("whatnext.pl/app/uploads", "cdn.whatnext.pl/uploads", $content);
		$content = str_replace("betawn.dkonto.pl/app/uploads", "cdn.whatnext.pl/uploads", $content);
		return $content;
	}
	return $content;
}

add_action('wp_head', 'schema');
function schema() {
	if (is_single()) {
		echo '
		<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "NewsArticle",
				"url": "'.get_permalink().'",
				"headline": "'.get_the_title().'",
				"image":[
						"'.get_the_post_thumbnail_url().'"
				],
				"datePublished":"'.get_the_time('c').'",
				"dateModified":"'.get_the_modified_time('c').'"
			}
    </script>';
	}
}