<?php
/**
 * Single view template.
 *
 * @package AMP
 */

/**
 * Context.
 *
 * @var AMP_Post_Template $this
 */
?>

	<?php 
		$disqus_identifier = $this->post->ID.' '.get_bloginfo( 'url' ).'/?p='.$this->post->ID;
		$url = $this->get( 'canonical_url' );
		$disqus_title = sanitize_title( $this->get( 'post_title' ) );
		$disqus_name = 'https://whatnextpl.disqus.com/embed.js#amp=1';
		$post = Timber::query_post('ThemePost'); 
		$author = $post->nrm_author();
	?>

	<?php $this->load_parts( [ 'html-start' ] ); ?>

	<?php $this->load_parts( [ 'header' ] ); ?>
	<div class="container">
		<article class="post">
			<div class="post__header" <?php echo $post->extra_thubnail(); ?>>
				<div class="row medium-uncollapse small-uncollapse large-uncollapse">
					<div class="large-12 small-12 medium-12 column">

						<?php if (!$post->extra() ) { ?>
							<div class="post__cat"><?php $post->top_level_categories(); ?></div>
						<?php } ?>
						
						<h1 class="post__title"><?php echo esc_html( $this->get( 'post_title' ) ); ?></h1>
						<div class="post__infos">
							<div class="post__info">
								<div class="post__author">
									<div class="post__author__img"><a href="<?php echo $author['link']; ?>">
										<amp-img alt="author"
											src="<?php echo $author['avatar']; ?>"
											width="30"
											height="30"
											layout="fixed">
										</amp-img>
									</a></div>
									<div class="post__author__txt">
										<div class="post__author__name"><a href="<?php echo $author['link']; ?>"><?php echo $author['name']; ?></a></div>
										<div class="post__date"><?php echo $post->nrm_date(); ?></div>
									</div>
								</div>
							</div>
							<div class="post__labels">
								<?php echo $post->product_placement(); ?>
								<?php echo $post->hot_title(); ?>
								<?php echo $post->brend(); ?>   
							</div>
						</div>
						

						<?php if (!$post->extra() ) { ?>
							<div class="post__thumbnail" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
						<?php } ?>

					</div>
				</div>
			</div>

			<div class="post__content">
				<?php echo $this->get( 'post_amp_content' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			</div>

			<div class="post__footer">
				<div class="post__share">
					<a class="btn btn--fb">Udostępnij na FB</a>
					<a class="btn btn--msg"><span>Wyślij przez Messenger</span></a>
					<a class="btn btn--twiter">
						<amp-img alt="twitter"
							src="<?php bloginfo( 'template_url' ); ?>/dist/images/twitter.svg"
							width="14"
							height="12"
							layout="fixed">
						</amp-img>
					</a>
				</div>
				<div class="post__tags">
					<div class="post__tags__title">TAGI:</div>
					<div class="post__tags__list">
						<?php 
							foreach ($post->tags() as $tag) {
								echo '<div class="label">'. $tag->name .'</div>';
							}
						?>
					</div>
				</div>
				<amp-iframe width=600 height=500
					layout="responsive"
					sandbox="allow-scripts allow-same-origin allow-modals allow-popups allow-forms"
					resizable
					style="background:#f4f6f8;"
					src="https://ocs-pl.oktawave.com/v1/AUTH_2887234e-384a-4873-8bc5-405211db13a2/spidersweb/2018/09/disqus.html?&url=<?php echo $url; ?>&disqus_title=<?php echo $disqus_title;?>&disqus_name=<?php echo $disqus_name; ?>">
					<div overflow tabindex="0" role="button" aria-label="Read more" style="background: #0088cc; color: #fff; border-radius: 4px; position: absolute; top: 50%; left: 50%; transform: translate(-50%); font-size: 12px; font-weight: 300; padding: 5px 20px"></div>
				</amp-iframe> 
			</div>
		</article>
	</div>

	<?php $this->load_parts( [ 'footer' ] ); ?>

	<?php $this->load_parts( [ 'html-end' ] ); ?>

</amp-script>
