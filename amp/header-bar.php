<?php
/**
 * Header bar template part.
 *
 * @package AMP
 */

/**
 * Context.
 *
 * @var AMP_Post_Template $this
 */

?>
<header class="header">
  <div class="row align-middle medium-uncollapse small-uncollapse large-uncollapse">
    <div class="columns shrink">
      <div class="header__logo">
        <a class="logo-dark" href="{{ site.url }}">
          <amp-img alt="logo"
            src="<?php bloginfo( 'template_url' ); ?>/dist/images/logo.svg"
            width="107"
            height="21"
            layout="fixed">
          </amp-img>
        </a>
        <a class="logo-white" href="{{ site.url }}">
          <amp-img alt="logo"
            src="<?php bloginfo( 'template_url' ); ?>/dist/images/logo-white.svg"
            width="107"
            height="21"
            layout="fixed">
          </amp-img>
        </a>
      </div>
    </div>
    <div class="columns">
      <div id="header__menu" class="header__menu">

        <div class="header__search logo-dark">
          <amp-img alt="search"
            src="<?php bloginfo( 'template_url' ); ?>/dist/images/search.svg"
            width="22"
            height="22"
            layout="fixed">
          </amp-img>
        </div>
        <div class="header__search logo-white">
          <amp-img alt="search"
            src="<?php bloginfo( 'template_url' ); ?>/dist/images/search-dark.svg"
            width="22"
            height="22"
            layout="fixed">
          </amp-img>
        </div>
        <div class="header__menu__items show-for-large">
          {% for item in menu_header.items %}
            <a class="{{ item.classes|join(' ') }}" href="{{ item.link }}">{{ item.title }}</a>
          {% endfor %}
        </div>
        <div class="header__search-form">
          {{ search_form }}
        </div>

        <button class="nav-button button-lines button-lines-x close hide-for-large">
          <span class="lines"></span>
        </button>

      </div>
    </div>
  </div>
  <div class="return-to-home js-p-r"><a href="{{ site.url }}">WRÓĆ DO STRONY </br>GŁÓWNEJ</a></div>
</header>

<nav class='nav-wrapper'>

  <div class="nav-wrapper__switcher">
    <div>Wybierz motyw:</div>
    <div class="switch-box">
      <input id="dark-mode-mobile" type="checkbox" class="switch">
    </div>
  </div>
  
  {% for item in menu_header.items %}
    <a class="{{ item.classes|join(' ') }}" href="{{ item.link }}">{{ item.title }}</a>
  {% endfor %}
  
  <a href="#" class="btn btn--big">Pokaz wszystkie wiadomości</a>
  
  <div class="footer__menu__title">{{ attribute(_context, 'footer_3').title }}</div>

  {% for item in attribute(_context, 'footer_3').items %}
    <a class="menu-item" href="{{ item.link }}">{{ item.title }}</a>
  {% endfor %}

</nav>