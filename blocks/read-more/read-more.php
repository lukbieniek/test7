<?php
add_action('acf/init', 'read_more');
function read_more() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'read_more',
      'title' => __('Read more'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'read_more_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function read_more_block_callback( $block ) {
  $context = Timber::context();
  $context['block'] = $block;
  $context['fields'] = get_fields();
  $context['posts']= Timber::query_posts( [
    'post__in' => get_field('posts'),
    'posts_per_page' => 3
  ], 'ThemePost' );
  Timber::render( 'blocks/read-more/read-more.twig', $context );
}
