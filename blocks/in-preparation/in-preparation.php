<?php
add_action('acf/init', 'in_preparation');
function in_preparation() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'in_preparation',
      'title' => __('W przygotowaniu'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'in_preparation_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function in_preparation_block_callback( $block ) {
  $context = Timber::context();
  $context['block'] = $block;
  $context['fields'] = get_fields();
  $context['posts']= Timber::query_posts( [
    'cat' => get_field('posts_category'),
    'posts_per_page' => 100,
    'post_status' => array('future')
  ], 'ThemePost' );
  Timber::render( 'blocks/in-preparation/in-preparation.twig', $context );
}
