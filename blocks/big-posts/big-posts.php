<?php
add_action('acf/init', 'b_post_and_sidebar');
function b_post_and_sidebar() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'b_post_and_sidebar',
      'title' => __('Big post z sideabr'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'b_post_and_sidebar_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function b_post_and_sidebar_block_callback( $block ) {
  $vars['display_date'] = true;
  $context['fields'] = get_fields();
  
  create_block($block, get_field("layout"), $vars, __DIR__);
}
