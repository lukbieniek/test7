@import 'colors';

body {
  font-family: "Gilroy", sans-serif;
  color: #ccc;
}

#firstPost {
  @include themed() {
    background-color: t('bg'); 
  }
}

.theme--dark {
  .logo-white {
    display: none;
  }
}

.theme--default {
  .logo-dark {
    display: none;
  }
}

.post-extra.theme--default .header__logo {
  .logo-white {
    display: none;
  }
  .logo-dark {
    display: block;
  }
}

.container {
  padding-top: rem-calc(92);
  position: relative;
  z-index: 0;
  padding-bottom: rem-calc(114);
  @include themed() {
    background-color: t('bg'); 
  }
  @include breakpoint(medium down) {
    padding-top: rem-calc(81);
    padding-bottom: 2.5rem;
  }
  &::after {
    content: '';
    min-height: rem-calc(304);
    @include breakpoint(medium down) {
      min-height: rem-calc(245);
    }
    position: absolute;
    width: 100%;
    @include themed() {
      background-color: t('secend-bg');
    }
    top: 92px;
    @include breakpoint(medium down) {
      top: rem-calc(81);
    }
    z-index: -2;
  }
}

.container.post--extra {
  padding-top: 0;
  &::after {
    display: none;
  }
}

.container--load {
  padding-top: 0;
  &::after {
    top: 0;
  }
}

.search {
  .container {
    &::after {
      height: rem-calc(77);
    }
  }
  &__titile {
    height: rem-calc(77);
    line-height: rem-calc(77);
    font-family: "Gilroy", sans-serif;
    font-weight: 700;
    font-size: 12px;
    text-transform: uppercase;
    @include themed() {
      color: t('third-text-color'); 
    } 
  }
}

.popular-topics {
  @include breakpoint(medium down) {
    margin-right: rem-calc(-20);
    display: none; // ???
  }
  .post__tags {
    border: 0;
    padding: 0;
    margin: 0 0 rem-calc(20) 0;
  }
}

input[type=text] {
  background-color: #0C0011;
  border: 1px solid #593568;
  box-sizing: border-box;
  border-radius: rem-calc(5);
  font-size: rem-calc(14);
  line-height: rem-calc(17);
  padding: rem-calc(17) rem-calc(24);
  color: #FFFFFF;
  font-family: "Gilroy", sans-serif;
  font-weight: 700;
  @include breakpoint(medium down) {
    font-size: rem-calc(16);
  }
}

.rwd-m {
  @include breakpoint(medium down) {
    margin-bottom: rem-calc(26);
  }
}

.archive {
  .medium-post {
    margin-top: rem-calc(26);
  }
  .home__title {
    margin-bottom: rem-calc(32);
  }
  .publication-counter .home__title {
    margin-top: 0;
    margin-bottom: 0;
  }
  .top-archive {
    margin-top: rem-calc(30);
    margin-bottom: rem-calc(32);
    @include breakpoint(medium down) {
      margin-right: -1.25rem;
      margin-left: -1.25rem;
    }
    .home__title {
      margin-top: 0;
      margin-bottom: rem-calc(15);
      @include breakpoint(medium down) {
        padding-right: 1.25rem;
        padding-left: 1.25rem;
        font-size: rem-calc(22);
      }
    }
    p {
      @include themed() {
        background-color: t('secend-bg');
      }
      margin-bottom: 0;
      @include themed() {
        color: t('text-color'); 
      } 
      font-size: 16px;
      line-height: 180%;
      font-family: 'Red Hat Display', sans-serif;
      @include breakpoint(medium up) {
        max-width: 75%;
      }
      @include breakpoint(medium down) {
        padding-right: 1.25rem;
        padding-left: 1.25rem;
        padding-bottom: rem-calc(26);
      }
    }
    .post-boks__tag {
      @include breakpoint(medium down) {
        padding-right: 1.25rem;
        padding-left: 1.25rem;
      }
    }
    img {
      width: 100%;
      height: rem-calc(260);
      object-fit: cover;
      object-position: center;
      border-radius: 5px;
      margin-top: rem-calc(30);
      @include breakpoint(medium down) {
        margin-top: -1rem;
      }
    }
    &__img {
      @include breakpoint(medium down) {
        padding-right: 1.25rem;
        padding-left: 1.25rem;
      }
    }
  }
  .top-archive--author {
    p {
      max-width: 100%;
      @include themed() {
        color: t('text-color'); 
      }
    }
  }
  .author-avatar {
    height: rem-calc(300);
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: rem-calc(60);
    margin-bottom: rem-calc(32);
    img {
      max-width: 100%;
      max-height: 100%;
      border-radius: 5px;
    }
  }
}

.wp-pagenavi {
  .btn.btn--big {
    height: rem-calc(64);
    line-height: rem-calc(64);
  }
  &__numbers {
    display: flex;
    justify-content: space-between;
    .btn--nr {
      width: rem-calc(64);
    }
    .btn.current {
      @include themed() {
        background-color: t('active');
      } 
      color: #FFFFFF;
      pointer-events: none;
      &:hover {
        @include themed() {
          background-color: t('pagination-active');
          color: t('secend-text-color');
        } 
      }
    }
    .last {
      width: rem-calc(128);
    }
  }
}

.page.home {
  .btn--more {
    max-width: unset;
    height: 3.625rem;
    line-height: 3.625rem;;
  }
  .container {
    padding-top: rem-calc(123);

  @include breakpoint(medium down) {
    padding-top: rem-calc(105);
  }
  }
}

#discussion {
  display: none;
}

#discussion.active {
  display: block;
}

.search-results {
  .publication-counter {
    display: none;
  }
}
.search__titile {
  margin-bottom: 3.375rem
}
.wp-pagenavi {
  .previouspostslink,
  .wp-pagenavi__numbers {
    margin-bottom: rem-calc(10);
  }
  .btn.btn--big {
    height: 3.625rem;
    line-height: 3.625rem;
  }
}