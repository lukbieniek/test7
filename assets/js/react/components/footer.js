import React from "react";


const footer = (props) => {

const { site,theme,1,i ,attribute(_context, 'footer_'~loop,i != 2{{ attribute(_context, 'footer_'~loop,item,i,twiter,twiter ,facebook,facebook  } = props;



return(

<footer className="footer">
  <div className="row large-collapse medium-uncollapse small-uncollapse">
    <div className="footer__menus footer__menus--acordion large-12 small-12 medium-12 column">
      <div className="footer__menu footer__menu--copy footer__menu_1">
        <div className="footer__logo">
          <a className="logo-dark" href={ site.url }><img src={`${ theme.link }/dist/images/logo.svg`} alt="logo" /></a>
          <a className="logo-white" href={ site.url }><img src=undefined/dist/images/logo-white.svg" alt="logo" /></a>
        </div>
        <div className="footer__copy">&copy; Wszelkie prawa zastrzeżone - {{ function('date', 'Y') }}</div>
      </div>

      {1..3.map((i, index ) => (

        <div className="footer__menu footer__menu_{{ i + 1 }}">

          <Choose><When condition={ attribute(_context, 'footer_'~loop.index).title }>

            <a className="footer__menu__title footer__menu__title--ac" data-menu-id="{{ i + 1 }}"><Choose><When condition={ i != 2 }>{{ attribute(_context, 'footer_'~loop.index).title }}{% endif }></a>    

          </When></Choose> 

          <Choose><When condition={ attribute(_context, 'footer_'~loop.index).items }>

            <ul className="footer__menu__list footer__menu__list_{{ i + 1 }}">

              {attribute(_context, 'footer_'~loop.index).items.map((item, index ) => (

                <li><a href={ item.link }>{ item.title }</a></li>

              ))}

            </ul> 

          </When></Choose>

          <Choose><When condition={ i == 1 }>

            <div className="footer__partner show-for-large">Wykonanie <a href="https://spiders.agency/">Spiders.agency</a></div>

          </When></Choose>

          <Choose><When condition={ i == 2 }>

            <div className="footer__partner show-for-large">Hosting <a href="https://dhosting.pl/">Dhosting</a></div>

          </When></Choose>

        </div>

      ))}

      <div className="footer__menu footer__menu_5">
        <div>
          <Choose><When condition={ twiter || facebook }>
            <div className="footer__menu__title">Dołącz do nas</div>
            <div className="footer__social">

              <Choose><When condition={ twiter }>
                <a href={ twiter } className="logo-dark" href=""><img src={`${ theme.link }/dist/images/twitter.svg`} alt="twit" /></a>
                <a href={ twiter } className="logo-white" href=""><img src=undefined/dist/images/twitter-dark.svg" alt="twit" /></a>
              </When></Choose>

              <Choose><When condition={ facebook }>
                <a href={ facebook } className="logo-dark" href=""><img src=undefined/dist/images/facebook-f.svg" alt="fb" /></a>
                <a href={ facebook } className="logo-white" href=""><img src=undefined/dist/images/facebook-f-dark.svg" alt="fb" /></a>
              </When></Choose>

            </div>
          </When></Choose>
        </div>
        <div className="footer__partner footer__partner--mobile">
          <div className="footer__menu__title">Wykonanie</div>
          <a href="https://spiders.agency/">Spiders.agency</a>
        </div>
      </div>

    </div>
  </div>
</footer>
)

}

export default footer;