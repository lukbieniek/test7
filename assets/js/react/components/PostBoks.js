import React from "react";


const PostBoks = (props) => {

const { post,not post } = props;



return(

<div className="post-boks">
  <div className="post-boks__header">
    <div className="post-boks__tag">{ post.nrm_top_level_categories }</div>
    <div className="post-boks__title"><a href={ post.link }>{ post.nrm_title }</a></div>
    { post.nrm_excerpt }
    { post.nrm_hot_title } 
  </div>
  <div className="post-boks__thumbnail" style={{ backgroundImage:`url(${ post.nrm_image_src.oryginal })` }}>

    <Choose><When condition={ post.meta('video_thumbnail') }>
      <div className="js-player" data-plyr-provider="youtube" data-plyr-embed-id="bTqVqk7FSmY"></div>
    </When></Choose>

    <div className="post-boks__extra">
      { post.nrm_product_placement }
      { post.nrm_brend }
    </div>

     <Choose><When condition={ not post.meta('video_thumbnail') }>
      <a href={ post.link }></a>
    </When></Choose>
    
  </div>
</div>
)

}

export default PostBoks;