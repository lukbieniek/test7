import React from "react";


const toparchive = (props) => {

const { term_page,term_description,term_description ,term_image,term_image  } = props;



return(

<div className="row large-uncollapse medium-uncollapse small-uncollapse">
  <div className="large-12 small-12 medium-12 column">
    <div className="top-archive">
      <div className="post-boks__tag">Kategoria</div>
      <div className="home__title">{ term_page.name }</div>

      <Choose><When condition={ term_description }>
        { term_description }
      </When></Choose>

      <Choose><When condition={ term_image }>
        <img src={ term_image } alt="">
      </When></Choose>

    </div>
  </div>
</div>
)

}

export default toparchive;