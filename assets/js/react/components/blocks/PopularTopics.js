import React from "react";


const PopularTopics = (props) => {

const { block,fields,tag } = props;



return(

<div id={ block.id } className="row large-uncollapse medium-uncollapse small-uncollapse" data-easygut-id="populartopics">
  <div className="large-12 small-12 medium-12 column">
    <div className="popular-topics">
      <div className="post__tags">
        <div className="post__tags__title">{ fields.title }</div>
        <div className="post__tags__list" data-simplebar>
          {fields.wybierz_tagi.map((tag, index ) => (
            <div className="label">{ tag.name }</div>
          ))}
        </div>
      </div>
    </div>
  </div>
</div>
)

}

export default PopularTopics;