import React from "react";


const ECommerce = (props) => {

const { block,fields } = props;



return(

<div id={ block.id } className="e-commerce" data-easygut-id="ecommerce">
  <img src={ fields.image } alt={ fields.name }>
  <div className="e-commerce__footer">
    <div className="e-commerce__name">{ fields.name }</div>
    <Choose><When condition={ fields.url }>
      <div><a className="btn btn--e-commerce" href={ fields.url }>Gdzie kupić?</a></div>
    </When></Choose>
  </div>
</div>
)

}

export default ECommerce;