import React from "react";


const SwipeGallery = (props) => {

const { block,fields,image } = props;



return(

<div id={ block.id } className="gallery swipe-gallery" itemscope itemtype="http://schema.org/ImageGallery" data-easygut-id="swipegallery">
  <ul className="SlickPhotoswipGallery">
  {fields.gallery_images .map((image, index ) => (
    <li><figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject"><a href={ image.url } itemprop="contentUrl" data-size=undefinedxundefined><img src={ image.url } itemprop="thumbnail" alt={ image.alt } /></a><figcaption itemprop="caption description">{ image.caption }</figcaption></figure></li>
  ))}
  </ul>
</div>
)

}

export default SwipeGallery;