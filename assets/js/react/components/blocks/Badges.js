import React from "react";


const Badges = (props) => {

const { block,theme,badge_type_name ,badge_type_icon  } = props;



return(

<div id={ block.id } className="badges" data-easygut-id="badges">
  <div className="badges__item badges__item--recommend">
    <div className="logo">
      <img src={`${ theme.link }/dist/images/logo.svg`} alt="logo">
    </div>
    <div className="badges__item__footer">
      <div className="badges__item__title">{ badge_type_name }</div>
      <div className="icon">
        <img src=undefined/dist/images/undefined alt="icon">
      </div>
    </div>
  </div>
</div>
)

}

export default Badges;