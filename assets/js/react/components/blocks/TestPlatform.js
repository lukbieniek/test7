import React from "react";


const TestPlatform = (props) => {

const { block,fields,rows,not row,row } = props;



return(

<div id={ block.id } className="test-platform" data-easygut-id="testplatform">
  <div className="test-platform__title">{ fields.title }</div>
  {rows.map((row, index ) => (
    <Choose><When condition={ not row.hide_row }>
      <div className="test-platform__item">
        <div className="key">{ row.key_1 }</div>
        <div className="value">{ row.value_1 }</div>
      </div>
    </When></Choose>
  ))}

</div>
)

}

export default TestPlatform;