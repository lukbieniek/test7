import React from "react";


const header = (props) => {

const { site,theme,dark_mode display: none; {% endif,menu_header,item,search_form ,attribute(_context, 'footer_3') } = props;



return(

<header className="header">
  <div className="row align-middle medium-uncollapse small-uncollapse large-uncollapse">
    <div className="columns shrink">
      <div className="header__logo">
        <a className="logo-dark" href={ site.url }><img src={`${ theme.link }/dist/images/logo.svg`} alt="logo" /></a>
        <a className="logo-white" href={ site.url }><img src=undefined/dist/images/logo-white.svg" alt="logo" /></a>
        <div className="switch-box" style="<Choose><When condition={ dark_mode }> display: none; {% endif }>">
          <input id="dark-mode" type="checkbox" className="switch">
        </div>
        <img className="switch-info" src={`${ theme.link }/dist/images/info.svg`} alt="info" />
        <div className="switch-info-box">
          <div className="switch-info-box__title">Dark Mode</div>
          Ten przycisk służy temu, by zmienić kolorystykę serwisu. Przetestuj i zostań z nami na dłużej ;)
        </div>
      </div>
    </div>
    <div className="columns">
      <div id="header__menu" className="header__menu">

        <div className="header__search logo-dark"><img src={`${ theme.link }/dist/images/search.svg`} alt="search" /></div>
        <div className="header__search logo-white"><img src=undefined/dist/images/search-dark.svg" alt="search" /></div>
        <div className="header__menu__items show-for-large">
          {menu_header.items.map((item, index ) => (
            <a className="{{ item.classNamees|join(' ') }}" href={ item.link }>{ item.title }</a>
          ))}
        </div>
        <div className="header__search-form">
          { search_form }
        </div>

        <button className="nav-button button-lines button-lines-x close hide-for-large">
          <span className="lines"></span>
        </button>

      </div>
    </div>
  </div>
  <div className="return-to-home js-p-r"><a href={ site.url }>WRÓĆ DO STRONY </br>GŁÓWNEJ</a></div>
</header>

<nav className='nav-wrapper'>

  <div className="nav-wrapper__switcher">
    <div>Wybierz motyw:</div>
    <div className="switch-box">
      <input id="dark-mode-mobile" type="checkbox" className="switch">
    </div>
  </div>
  
  {menu_header.items.map((item, index ) => (
    <a className="{{ item.classNamees|join(' ') }}" href={ item.link }>{ item.title }</a>
  ))}
  
  <a href="#" className="btn btn--big">Pokaz wszystkie wiadomości</a>
  
  <div className="footer__menu__title">{{ attribute(_context, 'footer_3').title }}</div>

  {attribute(_context, 'footer_3').items.map((item, index ) => (
    <a className="menu-item" href={ item.link }>{ item.title }</a>
  ))}

</nav>
)

}

export default header;