<?php
$context = Timber::get_context();
/**
 * Archive maping
 */
$archive_template = get_post(get_gutneberg_archive_template_id())->post_content;
$context['post']['content'] = render_blocks($archive_template);
if (!is_author()) {
  $tax_id = get_queried_object()->term_id;
  $archive_link = get_term_link($tax_id);
} else {
  $archive_link = get_author_posts_url(get_queried_object()->ID);
}

/**
 * archive.twig
 */
$term = new Timber\Term();
$context['term_page'] = $term;
$context['term_description'] = term_description($term->term_id);
$context['term_image'] = get_field('image', $term->taxonomy . '_' . $term->term_id);
/**
 * author.twig
 */
$user = new Timber\User( $wp_query->query_vars['author'] );
$context['user'] = $user;

if(get_alternative_avatar($user->ID)) {
  $context['user_avatar'] = get_alternative_avatar($user->ID);
} else {
  $context['user_avatar'] = get_avatar_url($user->ID, ['size' => '300']);
}

$context['wp_pagenavi'] = sw_wp_pagenavi($wp_query->query_vars['paged'], $archive_link );

Timber::render( 'views/templates/index.twig', $context );